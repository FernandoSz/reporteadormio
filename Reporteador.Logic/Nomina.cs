﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Data;

namespace Reporteador.Logic
{
    public class Nomina
    {
        public static string CadenaConexion { get; set; }
           /// <summary>
        /// Obtiene Todas las nominas de una compañia
        /// </summary>
        /// <param name="sUsuario">Esto es Usuario logeado.</param> 
        /// <param name="iIdCompania">Id de la compañia que se desea obtener.</param> 
        public static List<Entity.Nomina> ObtenNomina(int iIdCompania, string sUsuarioAcceso)
        {
            List<Entity.Nomina> lstNominas;
            DATNomina dNomina = new DATNomina();
            lstNominas = dNomina.ObtenNomina(iIdCompania, sUsuarioAcceso);
            return lstNominas;
        }

        /// <summary>
        /// Obtiene 1 nomina de una compañia
        /// </summary>
        /// <param name="sUsuario">Esto es Usuario logeado.</param> 
        /// <param name="iIdCompania">Id de la compañia que se desea obtener.</param> 
        /// /// <param name="iIdCompania">Id de la compañia que se desea obtener.</param> 
        public static Entity.Nomina ObtenNomina(int iIdCompania, int iIdNomina, string sUsuarioAcceso)
        {
            Entity.Nomina eNomina = null;
            DATNomina dNomina = new DATNomina();
            eNomina = dNomina.ObtenNomina(iIdCompania, iIdNomina, sUsuarioAcceso);
            return eNomina;
        }
    }
}
