﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Data;

namespace Reporteador.Logic
{
    public static class AutosExcesos
    {
        public static void InsertaAutosBaan(string strUsuario, string strProcess, string strPeriod, string strDocument, string strNumber, string strDate, string strTaxPeriod, string strReportingPeriod, string strReference, string strCur, string strAmountFC, string strAmountDebit, string strAmountCredit)
        {
            DATAutos dAutos = new DATAutos();   
            dAutos.InsertaAutosExcesosBaan(strUsuario, strProcess, strPeriod, strDocument, strNumber, strDate, strTaxPeriod, strReportingPeriod, strReference, strCur, strAmountFC, strAmountDebit, strAmountCredit);
        }

        public static void BorraAutosBaan(string strUsuario)
        {
            DATAutos dAutos = new DATAutos();
            dAutos.BorraAutosExcesosBaan(strUsuario);
        }

    }
}
