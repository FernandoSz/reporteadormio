﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Data;
using Reporteador.Entity;

namespace Reporteador.Logic
{
    public static class Compania
    {
        public static string CadenaConexion { get; set; }
        /// <summary>
        /// Obtiene Todas las compañias del usuario logeado
        /// </summary>
        /// <param name="sUsuario">Esto es Usuario logeado.</param>                 
        public static List<Entity.Compania> ObtenCompania(string sUsuarioAcceso)
        {
            List<Entity.Compania> lstCompanias;
            DATCompania dCompania = new DATCompania();
            lstCompanias = dCompania.ObtenCompania(sUsuarioAcceso);
            return lstCompanias;
        }

        /// <summary>
        /// Obtiene solo 1 compañia por Id
        /// </summary>
        /// <param name="sUsuario">Esto es Usuario logeado.</param> 
        /// <param name="iIdCompania">Id de la compañia que se desea obtener.</param> 
        public static Entity.Compania ObtenCompania(int iIdCompania, string sUsuarioAcceso)
        {
            Entity.Compania eCompania = null;
            DATCompania dCompania = new DATCompania();
            eCompania = dCompania.ObtenCompania(iIdCompania, sUsuarioAcceso);
            return eCompania;
        }

        #region MENSUAL


        /// <summary>
        /// Obtiene tipo de timbrado por periodo o mensual.
        /// </summary>
        /// <param name="iIdCompania"> Id de la compañia que se desea obtener..</param> 
        /// <param name="sUsuario">Esto es Usuario logeado.</param> 

        #endregion
    }
}
