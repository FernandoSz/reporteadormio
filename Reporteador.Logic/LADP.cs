﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.DirectoryServices.ActiveDirectory;

namespace Reporteador.Logic
{
    public class LADP
    {
        public static bool LoginActiveDirectory(string usuario, string dominio, string password, string ldap)
        {
            string strRootDN = string.Empty;
            DirectoryEntry objDseSearchRoot = null, objDseUserEntry = null;
            DirectorySearcher objDseSearcher = null;
            SearchResultCollection objResults = null;
            string strLDAPPath = string.Empty;
            try
            {

                strLDAPPath = ldap; //ejemplo "LDAP://192.168.0.243/DC=eslabon,DC=pruebas,DC=com";
                string strDomainname = dominio;
                objDseSearchRoot = new DirectoryEntry(strLDAPPath, strDomainname + "\\" + usuario, password, AuthenticationTypes.None);
                strRootDN = objDseSearchRoot.Properties["defaultNamingContext"].Value as string;
                objDseSearcher = new DirectorySearcher(objDseSearchRoot);
                objDseSearcher.CacheResults = false;
                objResults = objDseSearcher.FindAll();
                if (objResults.Count > 0)
                {
                    objDseUserEntry = objResults[0].GetDirectoryEntry();
                }

                if (objDseUserEntry == null)
                {

                    return false;
                }
            }

            catch (Exception ex)
            {
                return false; ;
            }
            finally
            {

            }
            return true;
        }
    }
}
