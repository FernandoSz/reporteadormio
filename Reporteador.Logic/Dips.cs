﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Data;
using Reporteador.Logic;

namespace Reporteador.Logic
{
    public static class Dips
    {
        public static string CadenaConexion { get; set; }
        /// <summary>
        /// Obtiene Todas las claves de movimiento
        /// </summary>  
        public static List<Entity.Dips> ObtenDips()
        {
            List<Entity.Dips> lstDips;
            DATDips dDips = new DATDips();
            lstDips = dDips.ObtenDips();
            return lstDips;
        }

    }
}
