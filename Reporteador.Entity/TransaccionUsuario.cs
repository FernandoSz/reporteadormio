﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class TransaccionUsuario
    {
        public int IdRolusuario { get; set; }
        public int IdRol { get; set; }
        public int IdTransaccion { get; set; }

        public string PermisoTransaccion { get; set; } 
        public string ClaveTransaccion { get; set; }
        public string DescripcioTransaccion { get; set; }

        public string Fuente { get; set; }
        public string Pagina { get; set; }
        public string Estatus { get; set; }
        public string Titulo { get; set; }

        public string ClaveRol { get; set; }
        public string DescripcionRol { get; set; }
        public string UsuarioAcceso { get; set; }        

        public string usuarioasignarol { get; set; }
        public bool checkbox { get; set; }

        public int IdMenu { get; set; }
        public string TituloMenu { get; set; }
        public string DescripcionMenu { get; set; }
    }
}
