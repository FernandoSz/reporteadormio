﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class TipoEmpleado
    {
        public int IdTipoEmpleado { get; set; }
        public string Descripcion { get; set; }
    }
}
