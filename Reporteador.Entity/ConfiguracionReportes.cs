﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class ConfiguracionReportes
    {
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public string Descripcion { get; set; }
        public string FechaCreacion { get; set; }
        public string Usuario { get; set; }
    }
}
