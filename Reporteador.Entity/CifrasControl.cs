﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class CifrasControl
    {
        public string PercepcionTotalGravado { get; set; }
        public string PercepcionTotalExcento { get; set; }
        public string DeduccionTotalGravado { get; set; }
        public string DeduccionTotalTotalExcento { get; set; }
        public string HorasExtraDobles { get; set; }
        public string HorasExtraTriples { get; set; }
        public string EmpleadoHorasExtraDobles { get; set; }
        public string EmpleadoHorasExtraTriples { get; set; }
        public string DiasIncapacidad { get; set; }
        public string EmpleadoIncapacidad { get; set; }
        public string TotalXMLsellar { get; set; }
        public string TotalXMLerrores { get; set; }
        public string TotalXMAnteriores { get; set; }
        //Otros Pagos
        public string OtrosPagosTotalGravado { get; set; }
        public string OtrosPagosTotalExento { get; set; }
        // HF Total ISR
        public string TotalIsr { get; set; }
    }
}
