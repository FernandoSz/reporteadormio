﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class CFDIDetalle12 : CFDIDetalle
    {
        // Acciones
        public decimal AccionesValorMercado { get; set; }
        public decimal AccionesPrecioAlOtorgarse { get; set; }

        // Horas Extras
        public string HorasExtrasDias { get; set; }
        public string HorasExtrasTipoHoras { get; set; }
        public string HorasExtras { get; set; }
        public decimal HorasExtrasImportePagado { get; set; }

        // Jubilaciones
        public decimal JubilacionTotalUnaExhibicion { get; set; }
        public decimal JubilacionTotalParcialidad { get; set; }
        public decimal JubilacionMontoDiario { get; set; }
        public decimal JubilacionIngresoAcumulable { get; set; }
        public decimal JubilacionIngresoNoAcumulable { get; set; }

        // Separacion
        public decimal SeparacionTotalPagado { get; set; }
        public int SeparacionAniosServicio { get; set; }
        public int SeparacionNumAniosServicio { get; set; }
        public decimal SeparacionUltimoSueldoMensOrd { get; set; }
        public decimal SeparacionIngresoAcumulable { get; set; }
        public decimal SeparacionIngresoNoAcumulable { get; set; }
        public decimal TotalSeparacionIndemnizacion { get; set; }

        public int EncabezadoNomina { get; set; }
        public int Cantidad { get; set; }
        public string Unidad { get; set; }
        public decimal Importe { get; set; }

        // Otros Pagos
        public string TipoOtroPago { get; set; }
        public decimal SubsidioCausado { get; set; }
        public decimal SaldoAFavor { get; set; }
        public int Anio { get; set; }
        public decimal RemanenteSalFav { get; set; }
        public decimal TotalSueldos { get; set; }

        //public decimal TotalGravado { get; set; }
        //public decimal TotalExento { get; set; }

        //public int AplicaAcciones { get; set; }
        //public int AplicaJubilacion { get; set; }

        public string Nodo { get; set; }
        public int Informativo { get; set; }

        public string TipoIncapacidad { get; set; }
        public string Agrupacion { get; set; }
    }
}
