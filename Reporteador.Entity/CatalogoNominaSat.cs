﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    class CatalogoNominaSat
    {
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public int IdCatalogo { get; set; }
        public string IdDetalle { get; set; }
        public string IdDetalleNomina { get; set; }
        public string  UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}
