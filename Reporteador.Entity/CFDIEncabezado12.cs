﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class CFDIEncabezado12 : CFDIEncabezado
    {
        public string EmisorCurp { get; set; }
        public string EmisorRegistroPatronal { get; set; }
        public string EmisorRfcPatronOrigen { get; set; }
        public string EntidadNSCFOrigenRecurso { get; set; }
        public decimal EntidadSNCFMontoRecursoPropio { get; set; }
        public string FechaFinalPago { get; set; }
        public string FechaInicialPago { get; set; }
        public decimal NumDiasPagados { get; set; }
        public string ReceptorAntiguedad { get; set; }
        public string ReceptorCurp { get; set; }
        public string ReceptorDepartamento { get; set; }
        public string ReceptorFechaInicioRelLaboral { get; set; }
        public string ReceptorNumSeguridadSocial { get; set; }
        public string ReceptorPuesto { get; set; }
        public string ReceptorRiesgoPuesto { get; set; }
        public string ReceptorSindicalizado { get; set; }
        public string ReceptorTipoContrato { get; set; }
        public string ReceptorTipoContratoNum { get; set; }
        public string ReceptorTipoJornada { get; set; }
        public string ReceptorTipoRegimen { get; set; }
        public string ReceptorNumEmpleado { get; set; }
        public string ReceptorClaveEntFed { get; set; } 
        public string TipoNomina { get; set; }
        public decimal TotalDeducciones { get; set; }
        public decimal TotalOtrosPagos { get; set; }
        public decimal TotalPercepciones { get; set; }
        public decimal DeduccionTotalOtrasDeducciones { get; set; }
        public decimal DeduccionTotalImpuestosRetenidos { get; set; }
        public decimal PerceptionTotalJubilacionPensionRetiro { get; set; }
        public decimal TotalSeparacionIndemnizacion { get; set; }
        public string SubContratacionRFCLabora { get; set; }
        public string SubContratacionPorcentajeTiempo { get; set; }
        public decimal TotalSueldos { get; set; }
        public decimal MuestraTotal { get; set; }
        public List<SubContratados> lstSubContratados { get; set; }

        // Hectorf: Comprobante 3.3
        // Objeto Comprobante.
        public string TipoDeComprobante { get; set; }
        public string Confirmacion { get; set; }
        public string CfdiRelacionadosTipoRelacion { get; set; }
        public string ReceptorRecidenciaFiscal { get; set; }
        public string ReceptorNumRegIdTrib { get; set; }
        public string ReceptorUsoCFDI { get; set; }
        public decimal TotalImpuestosRetenidos { get; set; }
        public decimal TotalImpuestosTrasladados { get; set; }

        // Objeto Concepto.
        public string ClaveProdServ { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string ClaveUnidad { get; set; }
        public string CuentaPredialNumero { get; set; }

        public string ConsecutivoNomina { get; set; }

    }

    public class SubContratados
    {
        public string RfcLabora { get; set; }
        public string PorcentajeTiempo { get; set; }
    }
}
