﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
   public class DetallePTU
    {
        public int Anio { get; set; }
        public int IdEmpleado { get; set; }
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public string Nomina { get; set; }
        public int ClaveMovimiento { get; set; }
        public string Dip { get; set; }
        public string DescripcionDip { get; set; }
        public decimal MontoDip { get; set; }
        public decimal MontoTotalPTU { get; set; }
        public decimal TotalAnticipoPTU { get; set; }
        public decimal RemanentePTU { get; set; }
        public decimal Impuesto { get; set; }
        public decimal Pension { get; set; }
        public decimal NetoPagarPTU { get; set; }
        public int IdPeriodo { get; set; }
        public DateTime FechaPeriodo { get; set; }        
    }
}
