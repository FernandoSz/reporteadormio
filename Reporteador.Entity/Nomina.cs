﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class Nomina
    {
        public int IdCompania { get; set; }
        public int IdNomina { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string Descripcion { get; set; }
        public float DiasDelPeriodo { get; set; }
        public string DipSueldo { get; set; }
        public string DipProporcion { get; set; }
        public float ProporcionSueldo { get; set; }
        public float ProporcionSeptimo { get; set; }
        public string ClaveCarga { get; set; }
        public List<Periodo>Periodos{ get; set; }

    }
}
