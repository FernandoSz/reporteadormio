﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class Compania
    {
        public int IdCompania { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string RazonSocial { get; set; }
        public string Rfc { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Colonia { get; set; }
        public string DelegacionMunicipio { get; set; }
        public string CodigoPostal { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
        public string Telefono { get; set; }
        public string Fax { get; set; }
        public string Www { get; set; }
        public string ClaveCarga { get; set; }
        public string NombreRepresentante { get; set; }
        public string RfcRepresentante { get; set; }
        public string CurpRepresentante { get; set; }
        public string ClaveEstado { get; set; }
        public string ActividadGiro { get; set; }
        public string NombreDirectorGeneral { get; set; }
        public string NombreDirectorRh { get; set; }
        //public string RegistroPatronal{ get; set; }
        //public string Municipio { get; set; }
        public List<Nomina> Nominas{ get; set; }

        #region MENSUAL
        public string TipoTimbrado { get; set; }
        #endregion

        public int IdNomina { get; set; }
        public double Porcentaje { get; set; }
        public int IdCompaniaOutsourcing { get; set; }
    }
}
