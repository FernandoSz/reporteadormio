﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporteador.Entity
{
    public class ClavesMovimiento
    {
        public int ClaveMovimiento { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string Descripcion { get; set; }
        public int TipoAcumulacion { get; set; }
        public int NominaNormal { get; set; }
        public int Compania { get; set; }
    }
}
