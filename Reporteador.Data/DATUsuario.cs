﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Entity;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;

namespace Reporteador.Data
{
    public class DATUsuario
    {
        #region " Constantes "
        const string NOMBRE_APLICATIVO = "CFDI";
        const string NOMBRE_ENSAMBLADO = "Reporteador.Data";
        const string NOMBRE_CLASE = "DATUsuario";
        #endregion

        private string DbProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }
        static Database namedDB = null;        
        public static string CadenaConexion { get; set; }
        public DATUsuario()
        {
            string sConexionesLic = ConfigurationManager.AppSettings["ConexionesLic"];
            if (sConexionesLic != string.Empty && sConexionesLic == "1")
            {
                namedDB = new GenericDatabase(Entity.SessionApp.CadenaConexion, DbProviderFactories.GetFactory(DbProvider));//EnterpriseLibraryContainer.Current.GetInstance<Database>(CadenaConexion);
            }
            else
            {
                if (!string.IsNullOrEmpty(CadenaConexion))
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>(CadenaConexion);
                else
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>("DefaultConnectionString");
            }
        }

        public Entity.Usuario ValidaUsuario(string sUsuarioAcceso,string sClave)
        {
            Entity.Usuario eUsuario = null;
            string storedProcName = "CFDI_VALIDA_USUARIO";

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "USUARIO", DbType.String, sUsuarioAcceso);
                    namedDB.AddInParameter(sprocCmd, "CLAVE", DbType.String, sClave);

                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {			
                            eUsuario = new Entity.Usuario();
                            eUsuario.UsuarioAcceso = reader["USUARIO_ACCESO"].ToString();
                            eUsuario.User = reader["USUARIO"].ToString();
                            eUsuario.FechaRegistro = DateTime.Parse(reader["FECHA_REGISTRO"].ToString());
                            eUsuario.FechaModificacion = DateTime.Parse(reader["FECHA_MODIFICACION"].ToString());
                            eUsuario.Grupo = Int32.Parse(reader["GRUPO"].ToString());
                            eUsuario.Descripcion = reader["DESCRIPCION"].ToString();
                            eUsuario.Clave = reader["CLAVE"].ToString();
                            eUsuario.NombreComputadora = reader["NOMBRE_COMPUTADORA"].ToString();                            
                            eUsuario.NombreCompleto = reader["NOMBRE_COMPLETO"].ToString();
                            eUsuario.DireccionCorreo = reader["DIRECCION_CORREO"].ToString();
                            eUsuario.Administrador = Int32.Parse(reader["ADMINISTRADOR"].ToString());
                            eUsuario.Acceso = Int32.Parse(reader["ACCESO"].ToString());
                            if (String.IsNullOrEmpty(reader["VALIDADOR"].ToString()))
                                eUsuario.Validador = Int32.Parse(reader["VALIDADOR"].ToString());
                            else
                                eUsuario.Validador = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eUsuario;
        }

        public Entity.Usuario ValidaUsuarioSE(string sUsuarioAcceso)
        {
            Entity.Usuario eUsuario = null;
            string storedProcName = "CFDI_VALIDA_USUARIO_SE";

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "USUARIO", DbType.String, sUsuarioAcceso);
                    
                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {			
                            eUsuario = new Entity.Usuario();
                            eUsuario.UsuarioAcceso = reader["USUARIO_ACCESO"].ToString();
                            eUsuario.User = reader["USUARIO"].ToString();
                            eUsuario.FechaRegistro = DateTime.Parse(reader["FECHA_REGISTRO"].ToString());
                            eUsuario.FechaModificacion = DateTime.Parse(reader["FECHA_MODIFICACION"].ToString());
                            eUsuario.Grupo = Int32.Parse(reader["GRUPO"].ToString());
                            eUsuario.Descripcion = reader["DESCRIPCION"].ToString();
                            eUsuario.Clave = reader["CLAVE"].ToString();
                            eUsuario.NombreComputadora = reader["NOMBRE_COMPUTADORA"].ToString();                            
                            eUsuario.NombreCompleto = reader["NOMBRE_COMPLETO"].ToString();
                            eUsuario.DireccionCorreo = reader["DIRECCION_CORREO"].ToString();
                            eUsuario.Administrador = Int32.Parse(reader["ADMINISTRADOR"].ToString());
                            eUsuario.Acceso = Int32.Parse(reader["ACCESO"].ToString());
                            eUsuario.Validador = Int32.Parse(reader["VALIDADOR"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eUsuario;
        }
    }
}
