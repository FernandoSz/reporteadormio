﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;

namespace Reporteador.Data
{
    public class DATParametro
    {
        #region " Constantes "
        const string NOMBRE_ENSAMBLADO = "Reporteador.Data.";
        const string NOMBRE_CLASE = "DATParametro.";
        #endregion

        private string DbProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }
        static Database namedDB = null;

        public DATParametro()
        {
            string sConexionesLic = ConfigurationManager.AppSettings["ConexionesLic"];
            if (sConexionesLic != string.Empty && sConexionesLic == "1")
            {
                namedDB = new GenericDatabase(Entity.SessionApp.CadenaConexion, DbProviderFactories.GetFactory(DbProvider));//EnterpriseLibraryContainer.Current.GetInstance<Database>(CadenaConexion);
            }
            else
            {
                if (Entity.SessionApp.CadenaConexion != "")
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>(Entity.SessionApp.CadenaConexion);
                else
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>("DefaultConnectionString");
            }
        }

        public bool isLADPActive(string sGrupo, string sVariable)
        {
            string storedProcName = "CFDI_OBTEN_PARAMETRO_LADP";
            int iResult = 0;
            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "GRUPO", DbType.String, sGrupo);
                    namedDB.AddInParameter(sprocCmd, "VARIABLE", DbType.String, sVariable);
                    namedDB.AddOutParameter(sprocCmd, "V_VALOR", DbType.String, 1000);

                    namedDB.ExecuteNonQuery(sprocCmd);
                    iResult = Convert.ToInt32(namedDB.GetParameterValue(sprocCmd, "V_VALOR"));
                    if (iResult == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error DB ObtenParametro: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            
        }
    }
}
