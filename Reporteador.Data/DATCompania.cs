﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Entity;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;

namespace Reporteador.Data
{
    public class DATCompania
    {
         #region " Constantes "
        const string NOMBRE_ENSAMBLADO = "Reporteador.Data.";
        const string NOMBRE_CLASE = "DATCompania.";
        #endregion

        private string DbProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }
        static Database namedDB = null;

        public DATCompania()
        {
            string sConexionesLic = ConfigurationManager.AppSettings["ConexionesLic"];
            if (sConexionesLic != string.Empty && sConexionesLic == "1")
            {
                namedDB = new GenericDatabase(Entity.SessionApp.CadenaConexion, DbProviderFactories.GetFactory(DbProvider));//EnterpriseLibraryContainer.Current.GetInstance<Database>(CadenaConexion);
            }
            else
            {
                if (Entity.SessionApp.CadenaConexion != "")
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>(Entity.SessionApp.CadenaConexion);
                else
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>("DefaultConnectionString");
            }
        }


        /// <summary>
        /// Obtiene Todas las compañias del usuario logeado
        /// </summary>
        /// <param name="sUsuario">Esto es Usuario logeado.</param>         
        public List<Entity.Compania> ObtenCompania(string sUsuarioAcceso)
        {
            string storedProcName = "CFDI_OBTEN_COMPANIAS";
            Entity.Compania eCompania;
            List<Entity.Compania> lstCompanias = new List<Compania>();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "ID_COMPANIA", DbType.Int32, 0);
                    namedDB.AddInParameter(sprocCmd, "USUARIO_ACCESO", DbType.String, sUsuarioAcceso);


                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {
                            eCompania = new Entity.Compania();
                            //eCompania.ActividadGiro = Functions.CheckStr(reader["ACTIVIDAD_GIRO"]);
                            //eCompania.Calle = Functions.CheckStr(reader["CALLE"]);
                            //eCompania.Ciudad = Functions.CheckStr(reader["CIUDAD"]);
                            //eCompania.ClaveCarga = Functions.CheckStr(reader["CLAVE_CARGA"]);
                            //eCompania.ClaveEstado = Functions.CheckStr(reader["CLAVE_ESTADO"]);
                            //eCompania.CodigoPostal = Functions.CheckStr(reader["CODIGO_POSTAL"]);
                            //eCompania.Colonia = Functions.CheckStr(reader["COLONIA"]);
                            //eCompania.CurpRepresentante = Functions.CheckStr(reader["CURP_REPRESENTANTE"]);
                            //eCompania.DelegacionMunicipio = Functions.CheckStr(reader["DELEG_O_MUNICIPIO"]);
                            //eCompania.Estado = Functions.CheckStr(reader["ESTADO"]);
                            //eCompania.Fax = Functions.CheckStr(reader["FAX"]);
                            //eCompania.FechaModificacion = Functions.CheckDate(reader["FECHA_MODIFICACION"]);
                            //eCompania.FechaRegistro = Functions.CheckDate(reader["FECHA_REGISTRO"]);
                            eCompania.IdCompania = Int32.Parse(reader["COMPANIA"].ToString());
                            ////eCompania.Municipio = Functions.CheckStr(reader["MUNICIPIO"]);
                            //eCompania.NombreDirectorGeneral = Functions.CheckStr(reader["NOMBRE_DIRECTOR_GRAL"]);
                            //eCompania.NombreDirectorRh = Functions.CheckStr(reader["NOMBRE_DIRECTOR_RH"]);
                            //eCompania.NombreRepresentante = Functions.CheckStr(reader["NOMBRE_REPRESENTANTE"]);
                            //eCompania.Numero = Functions.CheckStr(reader["NUMERO"]);
                            //eCompania.Pais = Functions.CheckStr(reader["PAIS"]);
                            eCompania.RazonSocial = reader["RAZON_SOCIAL"].ToString();
                            ////eCompania.RegistroPatronal = Functions.CheckStr(reader["REGISTRO_PATRONAL"]);
                            eCompania.Rfc = reader["RFC"].ToString();
                            //eCompania.RfcRepresentante = Functions.CheckStr(reader["RFC_REPRESENTANTE"]);
                            //eCompania.Telefono = Functions.CheckStr(reader["TELEFONO"]);
                            //eCompania.Usuario = Functions.CheckStr(reader["USUARIO"]);
                            //eCompania.Www = Functions.CheckStr(reader["WWW"]);

                            lstCompanias.Add(eCompania);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error DB ObtenCompania: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return lstCompanias;
        }

        /// <summary>
        /// Obtiene solo 1 compañia por Id
        /// </summary>
        /// <param name="sUsuario">Esto es Usuario logeado.</param> 
        /// <param name="iIdCompania">Id de la compañia que se desea obtener.</param> 
        public Entity.Compania ObtenCompania(int iIdCompania,string sUsuarioAcceso)
        {
            string storedProcName = "CFDI_OBTEN_COMPANIAS";
            Entity.Compania eCompania = null;
            
            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    namedDB.AddInParameter(sprocCmd, "id_compania", DbType.Int32, iIdCompania);
                    namedDB.AddInParameter(sprocCmd, "usuario_acceso", DbType.String, sUsuarioAcceso); 
                   
                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {
                            eCompania = new Entity.Compania();
                            //eCompania.ActividadGiro = Functions.CheckStr(reader["ACTIVIDAD_GIRO"]);
                            //eCompania.Calle = Functions.CheckStr(reader["CALLE"]);
                            //eCompania.Ciudad = Functions.CheckStr(reader["CIUDAD"]);
                            //eCompania.ClaveCarga = Functions.CheckStr(reader["CLAVE_CARGA"]);
                            //eCompania.ClaveEstado = Functions.CheckStr(reader["CLAVE_ESTADO"]);
                            //eCompania.CodigoPostal = Functions.CheckStr(reader["CODIGO_POSTAL"]);
                            //eCompania.Colonia = Functions.CheckStr(reader["COLONIA"]);
                            //eCompania.CurpRepresentante = Functions.CheckStr(reader["CURP_REPRESENTANTE"]);
                            //eCompania.DelegacionMunicipio = Functions.CheckStr(reader["DELEG_O_MUNICIPIO"]);
                            //eCompania.Estado = Functions.CheckStr(reader["ESTADO"]);
                            //eCompania.Fax = Functions.CheckStr(reader["FAX"]);
                            //eCompania.FechaModificacion = Functions.CheckDate(reader["FECHA_MODIFICACION"]);
                            //eCompania.FechaRegistro = Functions.CheckDate(reader["FECHA_REGISTRO"]);
                            eCompania.IdCompania = Int32.Parse(reader["COMPANIA"].ToString());
                            ////.Municipio = Functions.CheckStr(reader["MUNICIPIO"]);
                            //eCompania.NombreDirectorGeneral = Functions.CheckStr(reader["NOMBRE_DIRECTOR_GRAL"]);
                            //eCompania.NombreDirectorRh = Functions.CheckStr(reader["NOMBRE_DIRECTOR_RH"]);
                            //eCompania.NombreRepresentante = Functions.CheckStr(reader["NOMBRE_REPRESENTANTE"]);
                            //eCompania.Numero = Functions.CheckStr(reader["NUMERO"]);
                            //eCompania.Pais = Functions.CheckStr(reader["PAIS"]);
                            eCompania.RazonSocial = reader["RAZON_SOCIAL"].ToString();
                            ////eCompania.RegistroPatronal = Functions.CheckStr(reader["REGISTRO_PATRONAL"]);
                            eCompania.Rfc = reader["RFC"].ToString();
                            //eCompania.RfcRepresentante = Functions.CheckStr(reader["RFC_REPRESENTANTE"]);
                            //eCompania.Telefono = Functions.CheckStr(reader["TELEFONO"]);
                            //eCompania.Usuario = Functions.CheckStr(reader["USUARIO"]);
                            //eCompania.Www = Functions.CheckStr(reader["WWW"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error DB ObtenCompania: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return eCompania;
        }

    }
}
