﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Reporteador.Entity;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;

namespace Reporteador.Data
{
    public class DATClavesMovimiento
    {
        #region " Constantes "
        const string NOMBRE_APLICATIVO = "CFDI";
        const string NOMBRE_ENSAMBLADO = "Reporteador.Data";
        const string NOMBRE_CLASE = "DATClavesMovimiento";
        #endregion

        private string DbProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }
        static Database namedDB = null;

         public DATClavesMovimiento()
        {
            string sConexionesLic = ConfigurationManager.AppSettings["ConexionesLic"];
            if (sConexionesLic != string.Empty && sConexionesLic == "1")
            {
                namedDB = new GenericDatabase(Entity.SessionApp.CadenaConexion, DbProviderFactories.GetFactory(DbProvider));//EnterpriseLibraryContainer.Current.GetInstance<Database>(CadenaConexion);
            }
            else
            {
                if (Entity.SessionApp.CadenaConexion != "")
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>(Entity.SessionApp.CadenaConexion);
                else
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>("DefaultConnectionString");
            }
        }

        /// <summary>
        /// Obtiene Todas las claves de movimiento
        /// </summary>        
        public List<Entity.ClavesMovimiento> ObtenClavesMovimiento()
        {
            string storedProcName = "CFDI_OBTEN_CLAVESMOVIMIENTO";
            Entity.ClavesMovimiento eClavesMovimiento;
            List<Entity.ClavesMovimiento> lstClavesMovimientos = new List<ClavesMovimiento>();

            try
            {
                using (DbCommand sprocCmd = namedDB.GetStoredProcCommand(storedProcName))
                {
                    using (IDataReader reader = namedDB.ExecuteReader(sprocCmd))
                    {
                        while (reader.Read())
                        {
                            eClavesMovimiento = new Entity.ClavesMovimiento();
                            eClavesMovimiento.ClaveMovimiento = Int32.Parse(reader["CLAVE_MOVIMIENTO"].ToString());
                            eClavesMovimiento.Descripcion = reader["DESCRIPCION"].ToString();
                            eClavesMovimiento.FechaModificacion = DateTime.Parse(reader["FECHA_MODIFICACION"].ToString());
                            eClavesMovimiento.FechaRegistro = DateTime.Parse(reader["FECHA_REGISTRO"].ToString());
                            eClavesMovimiento.NominaNormal = Int32.Parse(reader["NOMINA_NORMAL"].ToString());
                            eClavesMovimiento.TipoAcumulacion = Int32.Parse(reader["TIPO_ACUMULACION"].ToString());
                            eClavesMovimiento.Usuario = reader["USUARIO"].ToString();                            
                            lstClavesMovimientos.Add(eClavesMovimiento);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error DB ObtenClavesMovimiento: " + NOMBRE_ENSAMBLADO + NOMBRE_CLASE + ex.Message);
            }
            return lstClavesMovimientos;
        }

    }
}
