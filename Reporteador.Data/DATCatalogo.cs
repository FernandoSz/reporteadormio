﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Reporteador.Entity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Configuration;

namespace Reporteador.Data
{
    public class DATCatalogo
    {
        #region " Constantes "
        const string NOMBRE_ENSAMBLADO = "Reporteador.Data.";
        const string NOMBRE_CLASE = "DATCatalogo.";
        #endregion

        private string DbProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }
        static Database namedDB = null;
        

        public DATCatalogo()
        {
            string sConexionesLic = ConfigurationManager.AppSettings["ConexionesLic"];
            if (sConexionesLic != string.Empty && sConexionesLic == "1")
            {
                namedDB = new GenericDatabase(Entity.SessionApp.CadenaConexion, DbProviderFactories.GetFactory(DbProvider));//EnterpriseLibraryContainer.Current.GetInstance<Database>(CadenaConexion);
            }
            else
            {
                if (Entity.SessionApp.CadenaConexion != "")
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>(Entity.SessionApp.CadenaConexion);
                else
                    namedDB = EnterpriseLibraryContainer.Current.GetInstance<Database>("DefaultConnectionString");
            }
        }


    }
}
