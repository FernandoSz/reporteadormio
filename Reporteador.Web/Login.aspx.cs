﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Reporteador.Entity;
using Reporteador.Logic;
using System.Configuration;
using System.Text;
using System.IO;

namespace Reporteador.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                cssLogin.Attributes.Add("href", "css/loginNegro.css");
            }
            else
            {
                cssLogin.Attributes.Add("href", "css/loginNegro.css");
            }
            
        }

        private string EliminaCaracter(string strCadena, string strCaracter)
        {
            int intPosicion;

            intPosicion = strCadena.IndexOf(strCaracter);

            while (intPosicion > -1)
            {
                strCadena = strCadena.Substring(0, intPosicion) + strCadena.Substring(intPosicion + 1);
                intPosicion = strCadena.IndexOf(strCaracter);
            }

            return strCadena;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            //string sUsuario = txtUsuario.Value;
            string sUsuario = txtUsuario.Text;
            string sClave = txtContrasena.Text;

            //Response.Redirect("~/Default.aspx", false);
            //Response.Redirect("~/content/Reporteador.Reportes/VisorReporte.aspx", false);

            Entity.Usuario eUsuario = new Entity.Usuario();
            //List<Entity.TransaccionUsuario> lstRol;
            try
            {
                //Validamos si utilizan seguridad extendida
                if (ConfigurationManager.AppSettings["AplicaSE"].ToString() == "1")
                {
                    eUsuario = Logic.Usuario.ValidaUsuarioSE(sUsuario);

                    if (eUsuario != null)
                    {
                        string sClaveEnc = EliminaCaracter(eUsuario.Clave, "\0");
                        if (sClave != Encriptar(sClaveEnc, false))
                        {
                            lblError.Text = "El usuario no está autenticado debidamente.";
                            throw new ApplicationException("El usuario no está autenticado debidamente.");
                        }
                    }
                    else
                        lblError.Text = "El usuario no está autenticado debidamente.";
                }
                else
                {

                    //Tipos de Login
                    int tipoLogin = Convert.ToInt32(ConfigurationManager.AppSettings["TipoLogin"]);
                    switch (tipoLogin)
                    {
                        case 1:

                            eUsuario = Logic.Usuario.ValidaUsuario(sUsuario, sClave);

                            break;
                        case 2:
                            if (Parametros.isLADPActive("GENERAL", "CONEXIONWINDOWS"))
                            {
                                string path = ConfigurationManager.AppSettings["LADP_PATH"].ToString();
                                string dominio = ConfigurationManager.AppSettings["LADP_DOMAIN"].ToString(); ;
                                if (!String.IsNullOrEmpty(path) && !String.IsNullOrEmpty(dominio))
                                {
                                    if (LADP.LoginActiveDirectory(sUsuario, dominio, sClave, path))
                                    {
                                        eUsuario = new Entity.Usuario();//Logic.Usuario.ValidaUsuario(sUsuario, sClave);
                                        eUsuario.UsuarioAcceso = sUsuario;
                                        eUsuario.Clave = sClave;
                                    }
                                    else
                                    {
                                        throw new ApplicationException("El usuario no está autenticado debidamente en Active Directory");
                                    }
                                }
                                else
                                {
                                    throw new ApplicationException("Agregue path y/o Dominio en el WEB Config para permitir acceso con Active Directory");
                                }
                            }
                            else
                            {
                                throw new ApplicationException("El parámetro de Active Directory no esta activado en la base de datos.");
                            }
                            break;
                        case 3:
                            if (Parametros.isLADPActive("GENERAL", "CONEXIONWINDOWS"))
                            {
                                string path = ConfigurationManager.AppSettings["LADP_PATH"].ToString();
                                string dominio = ConfigurationManager.AppSettings["LADP_DOMAIN"].ToString(); ;
                                if (!String.IsNullOrEmpty(path) && !String.IsNullOrEmpty(dominio))
                                {
                                    if (LADP.LoginActiveDirectory(sUsuario, dominio, sClave, path))
                                    {
                                        eUsuario = Logic.Usuario.ValidaUsuario(sUsuario, sClave);
                                    }
                                    else
                                    {
                                        throw new ApplicationException("El usuario no está autenticado debidamente en Active Directory");
                                    }
                                }
                                else
                                {
                                    throw new ApplicationException("Agregue path y/o Dominio en el WEB Config para permitir acceso con Active Directory");
                                }
                            }
                            else
                            {
                                eUsuario = Logic.Usuario.ValidaUsuario(sUsuario, sClave);
                            }
                            break;
                    }
                }


                if (eUsuario != null)
                {

                    //Response.Redirect("~/content/Reporteador.Proceso/Timbrar.aspx", false);
                    Sessions.InfoUsuario = eUsuario;
                    //Response.Redirect("~/content/Reporteador.Reportes/VisorReporte.aspx", false);
                    Response.Redirect("~/Default.aspx", false);
                }
                else
                {
                    lblError.Text = "El usuario no está autenticado debidamente.";
                    lblError.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Utilities.ManejaError(ex, Response, "~/Login.aspx");
            }
        }

        private string Encriptar(string sOriginal, bool bEncriptar)
        {
            string sClave = "{E61D35E1-558D-11D3-9B48-0060B0C3DAAA}";
            int iEncriptar128 = 0;
            try
            {
                iEncriptar128 = Convert.ToInt32(Logic.ParametrosCFDI.ObtenParametroNomina("GENERAL", "ENCRIPTAR128"));
                //iEncriptar128 = int.Parse(Utilerias.ObtenValorParametro("GENERAL", "ENCRIPTAR128", strConexion));
            }
            catch { }
            int iCaracteres = 255;
            if (iEncriptar128 == 1)
                iCaracteres = 127;

            int k = 0;
            int cO = 0;
            int cC = 0;
            string v = "";
            int I = 0;
            for (int J = 0; J < sOriginal.Length; J++)
            {

                if (I > sClave.Length)
                    I = 0;

                cO = (int)char.Parse(sOriginal.Substring(J, 1));
                if (cO > 255)
                    cO = Encoding.Default.GetBytes(sOriginal.Substring(J, 1))[0];
                cC = (int)char.Parse(sClave.Substring(I, 1));
                if (bEncriptar)
                {
                    k = cO + cC;
                    if (k > iCaracteres)
                        k = k - iCaracteres;
                }
                else
                {
                    k = cO - cC;
                    if (k < 0)
                        k = k + iCaracteres;
                }
                v += Convert.ToChar(k);
                I++;
            }
            return v;
        }
    }
}