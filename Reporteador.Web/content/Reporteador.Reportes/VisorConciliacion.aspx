﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VisorConciliacion.aspx.cs" Inherits="Reporteador.Web.content.Reporteador.Reportes.VisorConciliacion" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=15.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script>
        HideWait();
    </script>
    <title>Conciliaciones</title>
    <link href="../../css/normalize.css" rel="stylesheet" type="text/css" />
    <link id="cssGeneral" rel="stylesheet" type="text/css" />
    <link id="cssMenu" rel="stylesheet" type="text/css" />
    <link id="cssGrid" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style1 {
            width: 120px;
            height: 3px;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            width: 84px;
            height: 23px;
        }
        .auto-style5 {
            height: 5px;
            width: 94px;
        }
        .auto-style6 {
            width: 94px;
        }
        .auto-style7 {
            width: 94px;
            height: 23px;
        }
        .auto-style8 {
            width: 84px;
        }
        .auto-style10 {
            height: 5px;
            width: 303px;
        }
        .auto-style12 {
            width: 303px;
            height: 23px;
        }
        .auto-style20 {
            height: 15px;
        }
        .auto-style23 {
            cursor: pointer;
            margin-left: 0;
        }
        .auto-style24 {
            width: 100%;
        }
        .auto-style26 {
            width: 96px;
        }
        .auto-style27 {
            height: 15px;
            width: 96px;
        }
        .auto-style30 {
            width: 126px;
        }
        .auto-style29 {
            height: 21px;
            width: 126px;
        }
        .auto-style25 {
            height: 21px;
        }
        .auto-style31 {
            width: 126px;
            height: 23px;
        }
        .auto-style33 {
            height: 3px;
        }
        .auto-style34 {
            width: 84px;
            height: 3px;
        }
        .auto-style35 {
            width: 94px;
            height: 3px;
        }
        .auto-style36 {
            width: 303px;
            height: 3px;
        }
        .auto-style32 {
            font-weight: bold;
        }
        .auto-style37 {
            height: 26px;
        }
        </style>
</head>
<body style="background: #fff;color: #bebebe;
	font-family: Tahoma, Arial, Helvetica, sans-serif;
	font-size: 11px;
	margin: 0;">
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"
     AsyncPostBackTimeout="7200" EnableScriptGlobalization="True">
    </ajaxToolkit:ToolkitScriptManager>
    
      <div id="main">
    <div class="full_w">
                <div class="h_title">
                </div>
                <h1>
                    <asp:HiddenField ID="hfIdReporte" runat="server" />
                    <asp:Label ID="lblNombreReporte" runat="server"></asp:Label>
                </h1>
                <div class="margen_container">                    
                         <table>
                <tr>
                    <td class="auto-style1" style="border-bottom-style: groove">
                        <strong>GENERALES</strong></td>
                    <td class="auto-style33" style="border-bottom-style: groove">
                        </td>
                    <td class="auto-style34" style="border-bottom-style: groove">
                        </td>
                    <td class="auto-style1" style="border-bottom-style: groove">
                        ARCHIVOS</td>
                    <td class="auto-style33" style="border-bottom-style: groove">
                        </td>
                    <td class="auto-style35" style="border-bottom-style: groove">
                        </td>
                    <td class="auto-style35" style="border-bottom-style: groove">
                        <strong>PERIODOS</strong></td>
                    <td class="auto-style36" style="border-bottom-style: groove">
                        </td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="2">
                    </td>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td colspan="2" style="vertical-align: top;">
                        &nbsp;</td>
                    <td class="auto-style7">
                        &nbsp;</td>
                    <td class="auto-style7">
                        &nbsp;</td>
                    <td class="auto-style12">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" rowspan="5" style="vertical-align: top">
                        <table class="auto-style24">
                            <tr>
                                <td class="auto-style30">
                        <span>Compañia:</span></td>
                                <td>
                        <asp:DropDownList ID="ddlCompania" runat="server" ValidationGroup="buscar" CssClass="ddl_css" DataTextField="RazonSocial"
                            Width="300" DataValueField="IdCompania" OnSelectedIndexChanged="ddlCompania_SelectedIndexChanged" AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvCompania" runat="server" ControlToValidate="ddlCompania"
                            Display="Dynamic" ValidationGroup="buscar" ForeColor="Red" InitialValue="0" Text="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style29"><span>Nómina:</span></td>
                                <td class="auto-style25">
                        <asp:DropDownList ID="ddlNomina" runat="server" CssClass="ddl_css" ValidationGroup="buscar" DataTextField="Descripcion"
                            Width="300" DataValueField="IdNomina">
                        </asp:DropDownList>
                               <asp:RequiredFieldValidator ID="rfvNomina" runat="server" ControlToValidate="ddlNomina"
                            Display="Dynamic" ValidationGroup="buscar" ForeColor="Red" InitialValue="0" Text="*"></asp:RequiredFieldValidator>     
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style31">
                        <span>Clave Movimiento:</span></td>
                                <td class="auto-style2">
                    <asp:DropDownList ID="ddlClaveMovi" runat="server" CssClass="ddl_css" DataTextField="Descripcion"
                            Width="300" DataValueField="ClaveMovimiento" ValidationGroup="buscar">
                        </asp:DropDownList>                        
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style30">&nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style30">Dip:</td>
                                <td>
                    <asp:DropDownList ID="ddlDip" runat="server" CssClass="ddl_css" DataTextField="DipDescripcion"
                            Width="300px" DataValueField="Dip" Height="19px">
                        </asp:DropDownList>
                           <asp:RequiredFieldValidator ID="rfvDip" runat="server" ControlToValidate="ddlDip"
                            Display="Dynamic" ValidationGroup="buscar" ForeColor="Red" InitialValue="0" Text="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style30">&nbsp;</td>
                                <td>
                                    <asp:RadioButtonList ID="RdoBtnLstAutos" runat="server">
                                        <asp:ListItem Selected="True" Value="Autos"> Autos</asp:ListItem>
                                        <asp:ListItem Value="Excesos"> Excesos</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="auto-style3">
                    </td>
                    <td colspan="2" rowspan="5" style="vertical-align: top;">
                        <table class="auto-style24">
                            <tr>
                                <td style="vertical-align: top">Archivo Baan:</td>
                                <td style="vertical-align: top">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                <td>
                                    <asp:FileUpload ID="uplTheFile" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style20"></td>
                                <td class="auto-style20">&nbsp;</td>
                                <td class="auto-style20"></td>
                            </tr>
                            <tr>
                                <td>
                                    Nombre Archivo Salida:</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                
                                    <asp:TextBox ID="txtNombreArchivo" runat="server" MaxLength="10" CssClass="auto-style23" Height="16px" Width="184px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvArchivo" runat="server" ControlToValidate="txtNombreArchivo"
                                    Display="Dynamic" ValidationGroup="buscar" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="auto-style7">
                        </td>
                    <td colspan="2" rowspan="5" style="vertical-align: top">
                        <table class="auto-style24">
                            <tr>
                                <td class="auto-style26">
                                    Año:</td>
                                <td colspan="2">
                    <asp:DropDownList ID="ddlAnio" runat="server" CssClass="ddl_css" Width="93px" Height="16px" style="margin-left: 0">
                            <asp:ListItem Selected="True">2018</asp:ListItem>
                            <asp:ListItem>2017</asp:ListItem>                            
                            <asp:ListItem>2016</asp:ListItem>
                        </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style27">Mes:</td>
                                <td class="auto-style20" colspan="2">
                    <asp:DropDownList ID="ddlMes" runat="server" CssClass="ddl_css" Width="148px" Height="16px" style="margin-left: 0">
                            <asp:ListItem Value="1">Enero</asp:ListItem>
                            <asp:ListItem Value="2">Febrero</asp:ListItem>                            
                            <asp:ListItem Value="3">Marzo</asp:ListItem>
                            <asp:ListItem Value="4">Abril</asp:ListItem>
                            <asp:ListItem Value="5">Mayo</asp:ListItem>
                            <asp:ListItem Value="6">Junio</asp:ListItem>
                            <asp:ListItem Value="7">Julio</asp:ListItem>
                            <asp:ListItem Value="8">Agosto</asp:ListItem>
                            <asp:ListItem Value="9">Septiembre</asp:ListItem>
                            <asp:ListItem Value="10">Octubre</asp:ListItem>
                            <asp:ListItem Value="11">Noviembre</asp:ListItem>
                            <asp:ListItem Value="12">Diciembre</asp:ListItem>
                        </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style26">
                                    Fechas</td>
                                <td>
                                
                                    Desde:</td>
                                <td>
                                
                                    <asp:TextBox ID="txtFechaDesde" runat="server" MaxLength="10" CssClass="auto-style23"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaDesde"
                                        Format="dd/MM/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="rfvFechaDesde" runat="server" ControlToValidate="txtFechaDesde"
                                    Display="Dynamic" ValidationGroup="buscar" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style26">&nbsp;</td>
                                <td>Hasta:</td>
                                <td><asp:TextBox ID="txtFechaHasta" runat="server" MaxLength="10" CssClass="auto-style23"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="txtFechaHasta_CalendarExtender" runat="server" TargetControlID="txtFechaHasta"
                                        Format="dd/MM/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="rfvFechaHasta" runat="server" ControlToValidate="txtFechaHasta"
                                    Display="Dynamic" ValidationGroup="buscar" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style7">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style8">
                    </td>
                    <td class="auto-style6">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style8">
                        &nbsp;</td>
                    <td class="auto-style6">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        
                    </td>
                    <td class="auto-style7">
                        </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        &nbsp;</td>
                    <td class="auto-style2">
                        &nbsp;</td>
                    <td class="auto-style3">
                        
                        &nbsp;</td>
                    <td class="auto-style2">
                        &nbsp;</td>
                    <td class="auto-style2">
                        &nbsp;</td>
                    <td class="auto-style7">
                        &nbsp;</td>
                    <td class="auto-style7">
                        &nbsp;</td>
                    <td class="auto-style12">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 5px" colspan="5">
                        &nbsp;</td>
                    <td class="auto-style5">
                        &nbsp;</td>
                    <td class="auto-style5">
                        &nbsp;</td>
                    <td class="auto-style10">
                        &nbsp;</td>
                </tr>
                </table>
                         <table class="auto-style24">
                             <tr>
                                 <td>
                                     <table class="auto-style24">
                                         <tr>
                                             <td class="auto-style20">F<span class="auto-style32">ORMATO</span></td>
                                             <td class="auto-style20"></td>
                                         </tr>
                                         <tr>
                                             <td style="border-style: double none none none">&nbsp;</td>
                                             <td>&nbsp;</td>
                                         </tr>
                                         <tr>
                                             <td class="auto-style37">
                                                 <asp:CheckBox ID="ChkBoxBaan" runat="server" Text=" Incluir Totales Baan en Resumen" />
                                             </td>
                                             <td class="auto-style37"></td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 <asp:CheckBox ID="ChkBoxAuxBaan" runat="server" Checked="True" Text=" Incluir Auxiliar Baan" />
                                             </td>
                                             <td>&nbsp;</td>
                                         </tr>
                                     </table>
                                 </td>
                             </tr>
                         </table>
                         <br />
                <div class="h_title">
                </div>
                </div>
            </div>
           
    <div>
    </div>
    <div>
           
    </div>
    </div>
        <p>
                        <asp:Button ID="btnBuscar" runat="server" Text="Conciliar" CssClass="buton btn_buscar"
                            Width="100" OnClick="btnBuscar_Click" ValidationGroup="buscar" />
        </p>
    </form>
</body>
</html>
