﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using System.Globalization;
using Reporteador.Logic;
//using Reporteador.Logic;

namespace Reporteador.Web.content.Reporteador.Reportes
{
    public partial class VisorReporte : System.Web.UI.Page
    {
        public string ID_REPORTE
        {
            get { return Convert.ToString(ViewState["vsIdReporte"]); }
            set { ViewState["vsIdReporte"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //SeleccionEmpleados1.RecargaPagina += new EventHandler(SeleccionEmpleados1_RecargaPagina);
            //SeleccionEstructuras1.RecargaPagina += new EventHandler(SeleccionEmpleados1_RecargaPagina);


            //cssGeneral.Attributes.Add("href", "~/css/cssNegro.css");
            //cssMenu.Attributes.Add("href", "~/css/MenuNegro.css");
            //cssGrid.Attributes.Add("href", "~/css/gridNegro.css");

            if (!Page.IsPostBack)
            {
                try
                {
                    cargaComboCompania(Sessions.InfoUsuario.UsuarioAcceso);
                    cargaComboClavesMovimiento();
                    //Logic.Empleado.EliminaEmpleadoSeleccion(Sessions.InfoUsuario.UsuarioAcceso);
                    //Logic.Estructura.EliminaNivelMasivo(Sessions.InfoUsuario.UsuarioAcceso);
                }
                catch (Exception ex)
                {
                    Utilities.ManejaError(ex, Response, "~/Default.aspx");
                }

                
            }
        }
        protected void SeleccionEmpleados1_RecargaPagina(object sender, EventArgs e)
        {
            try
            {
                CargaReporte();
            }
            catch (Exception ex)
            {
                Utilities.ManejaError(ex, Response, "~/content/Reporteador.Reportes/VisorReporte.aspx");
            }
        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ReportViewerTotales.Reset();
                CargaReporte();
            }
            catch (Exception ex)
            {
                Utilities.ManejaError(ex, Response, "~/content/Reporteador.Reportes/VisorReporte.aspx");
            }
        }

        private void CargaReporte()
        {
            switch (RdoBtnLstAgrupacion.SelectedItem.Value)
            {
                case "TotalGeneral":
                    ReportViewerTotales.LocalReport.ReportPath = @"content\Reporteador.Reportes\CFDI\Totales\Totales_CFDI.rdlc";
                    break;
                case "TotalCia":
                    ReportViewerTotales.LocalReport.ReportPath = @"content\Reporteador.Reportes\CFDI\Totales\Totales_Cia_CFDI.rdlc";
                    break;
                case "Detalle":
                    switch (ddlEstatus.SelectedValue)
                    {
                        case "1":
                            ReportViewerTotales.LocalReport.ReportPath = @"content\Reporteador.Reportes\CFDI\Detalle\Detalle_CFDI.rdlc";
                            break;
                        case "2":
                            ReportViewerTotales.LocalReport.ReportPath = @"content\Reporteador.Reportes\CFDI\Detalle\Detalle_Cancelaciones_CFDI.rdlc";
                            break;
                        case "3":
                        case "4":
                            ReportViewerTotales.LocalReport.ReportPath = @"content\Reporteador.Reportes\CFDI\Detalle\Detalle_Validados_CFDI.rdlc";
                            break;
                        case "5":
                            ReportViewerTotales.LocalReport.ReportPath = @"content\Reporteador.Reportes\CFDI\Detalle\Detalle_Acumulados_CFDI.rdlc";
                            break;
                        case "6":
                            ReportViewerTotales.LocalReport.ReportPath = @"content\Reporteador.Reportes\CFDI\Detalle\Detalle_Recuperaciones_CFDI.rdlc";
                            break;
                        case "7":
                            ReportViewerTotales.LocalReport.ReportPath = @"content\Reporteador.Reportes\CFDI\Detalle\Detalle_Expatriados_CFDI.rdlc";
                            break;
                    }
                    break;
            }

            DataSet ds = new DataSet();
            ds = GeneraDataSet();
            if (ds != null && ds.Tables.Count > 0)
            {
                ReportViewerTotales.LocalReport.DataSources.Clear();
                ReportDataSource dataset = new ReportDataSource("DataSet1", ds.Tables[0]);
                ReportViewerTotales.LocalReport.DataSources.Add(dataset);
                ReportViewerTotales.SizeToReportContent = true;
                ReportViewerTotales.Height = Unit.Percentage(100);
                ReportViewerTotales.Width = Unit.Percentage(100);
                ReportViewerTotales.ZoomMode = ZoomMode.PageWidth;
                ReportViewerTotales.ShowBackButton = true;
                ReportViewerTotales.LocalReport.Refresh();
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Key", "alert('No se encontró información con los filtros seleccionados.')", true);
        }

        private void cargaComboCompania(string sUsuarioAcceso)
        {
            //Cargamos Companias
            //Logic.Compania lCompania = new Logic.Compania();
            List<Entity.Compania> lstCompanias;

            lstCompanias = Logic.Compania.ObtenCompania(Sessions.InfoUsuario.UsuarioAcceso);
            ddlCompania.DataSource = lstCompanias;
            ddlCompania.DataBind();

            ddlCompania.Items.Insert(0, new ListItem("Todas", "0"));
            ddlCompania.SelectedIndex = 0;

        }

        private void cargaComboNomina(int iCompania)
        {
            //Logic.Nomina lNomina = new Nomina();
            List<Entity.Nomina> lstNominas;

            lstNominas = Logic.Nomina.ObtenNomina(iCompania, Sessions.InfoUsuario.UsuarioAcceso);
            ddlNomina.DataSource = lstNominas;
            ddlNomina.DataBind();

            ddlNomina.Items.Insert(0, new ListItem("Todas", "0"));
            ddlNomina.SelectedIndex = 0;
        }

        private void cargaComboClavesMovimiento()
        {
            //Logic.ClavesMovimiento lClavesMovimiento = new Logic.ClavesMovimiento();
            List<Entity.ClavesMovimiento> lstClavesMovimientos;

            lstClavesMovimientos = Logic.ClavesMovimiento.ObtenClavesMovimiento();
            ddlClaveMovi.DataSource = lstClavesMovimientos;
            ddlClaveMovi.DataBind();

            ddlClaveMovi.Items.Insert(0, new ListItem("Todas", "0"));
            ddlClaveMovi.SelectedIndex = 0;
        }

        private void limpiaCombos(string sID)
        {
            switch (sID)
            {
                case "ddlCompania":
                    ddlNomina.Items.Clear();
                    break;
                case "ddlNomina":
                    break;
            }
        }

        private DataSet GeneraDataSet()
        {
            DateTime dFechaDesde;
            DateTime dFechaHasta;
            int iIdNomina;
            int iIDClaveMovimiento;
            string sFiltroPeriodo;

            DataSet ds = new DataSet();
            int iIdCompania = Convert.ToInt32(ddlCompania.SelectedValue);
            if (String.IsNullOrEmpty(ddlNomina.SelectedValue))
                iIdNomina = 0;
            else
                iIdNomina = Convert.ToInt32(ddlNomina.SelectedValue);

            if (String.IsNullOrEmpty(ddlClaveMovi.SelectedValue))
                iIDClaveMovimiento = 0;
            else
                iIDClaveMovimiento = Convert.ToInt32(ddlClaveMovi.SelectedValue);

            string sEstatus = ddlEstatus.SelectedValue;

            if (String.IsNullOrEmpty(txtFechaDesde.Text))
            {
                dFechaDesde = DateTime.Today.Date;
                dFechaHasta = DateTime.Today.Date;
            }
            else
            {
                dFechaDesde = DateTime.ParseExact(txtFechaDesde.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);
                dFechaHasta = DateTime.ParseExact(txtFechaHasta.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);
            }

            if (RdoBtnAnio.Checked == true)
                sFiltroPeriodo = "anio";
            else
                sFiltroPeriodo = "fechas";


            if (dFechaDesde > dFechaHasta)
                throw new ApplicationException("La fecha desde no puede ser mayor a la fecha hasta.");

            switch (RdoBtnLstAgrupacion.SelectedItem.Value)
            {
                case "TotalGeneral":
                    ds = Logic.Reportes.RptTotalesCFDI(Convert.ToInt32(ddlAnio.SelectedValue), iIdCompania, iIdNomina, iIDClaveMovimiento, dFechaDesde, dFechaHasta, sFiltroPeriodo);
                    break;
                case "TotalCia":
                    ds = Logic.Reportes.RptTotalesCiaCFDI(Convert.ToInt32(ddlAnio.SelectedValue), iIdCompania, iIdNomina, iIDClaveMovimiento, dFechaDesde, dFechaHasta, sFiltroPeriodo);
                    break;
                case "Detalle":
                    switch (sEstatus)
                    {
                        case "1":
                            ds = Logic.Reportes.RptDetalleErroresCFDI(Convert.ToInt32(ddlAnio.SelectedValue), iIdCompania, iIdNomina, iIDClaveMovimiento, dFechaDesde, dFechaHasta, sFiltroPeriodo);
                            break;
                        case "2":
                            ds = Logic.Reportes.RptDetalleCancelaCFDI(Convert.ToInt32(ddlAnio.SelectedValue), iIdCompania, iIdNomina, iIDClaveMovimiento, dFechaDesde, dFechaHasta, sFiltroPeriodo);
                            break;
                        case "3":
                            ds = Logic.Reportes.RptDetalleValidadosCFDI(Convert.ToInt32(ddlAnio.SelectedValue), iIdCompania, iIdNomina, iIDClaveMovimiento, "VA", dFechaDesde, dFechaHasta, sFiltroPeriodo);
                            break;
                        case "4":
                            ds = Logic.Reportes.RptDetalleValidadosCFDI(Convert.ToInt32(ddlAnio.SelectedValue), iIdCompania, iIdNomina, iIDClaveMovimiento, "V", dFechaDesde, dFechaHasta, sFiltroPeriodo);
                            break;
                        case "5":
                            ds = Logic.Reportes.RptDetalleAcumuladosCFDI(Convert.ToInt32(ddlAnio.SelectedValue), iIdCompania, iIdNomina, iIDClaveMovimiento, "VA", dFechaDesde, dFechaHasta, sFiltroPeriodo);
                            break;
                        case "6":
                            ds = Logic.Reportes.RptDetalleRecuperacionesCFDI(Convert.ToInt32(ddlAnio.SelectedValue), iIdCompania, iIdNomina, iIDClaveMovimiento, "VA", dFechaDesde, dFechaHasta, sFiltroPeriodo);
                            break;
                        case "7":
                            ds = Logic.Reportes.RptDetalleExpatriadosCFDI(Convert.ToInt32(ddlAnio.SelectedValue), iIdCompania, iIdNomina, iIDClaveMovimiento, "VA", dFechaDesde, dFechaHasta, sFiltroPeriodo);
                            break;
                    }
                    break;
            }

            //if (RdoBtnLstAgrupacion.SelectedItem.Value == "TotalGeneral" || RdoBtnLstAgrupacion.SelectedItem.Value == "TotalCia")
            //    ds = Logic.Reportes.RptTotalesCFDI(Convert.ToInt32(ddlAnio.SelectedValue));
            //else
            //    ds = Logic.Reportes.RptDetalleErroresCFDI(Convert.ToInt32(ddlAnio.SelectedValue));

            return ds;
            
        }

        protected void ddlCompania_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReportViewerTotales.Reset();
            if (ddlCompania.SelectedValue != "0")
            {
                //Obtenemos las nominas de la compania seleccionada
                try
                {
                    limpiaCombos(ddlCompania.ID);
                    cargaComboNomina(Convert.ToInt32(ddlCompania.SelectedValue));
                }
                catch (Exception ex)
                {
                    Utilities.ManejaError(ex, Response, "~/content/Reporteador.Consultas/ConsultaEstatus.aspx");
                    //MBoxx.ShowError("Error al intentar cargar los combos, Error: "+ EX.Message.ToString(), "Error", 150, 300);
                }
            }
            else
            {
                limpiaCombos(ddlCompania.ID);
            }
        }

        protected void RdoBtnLstAgrupacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReportViewerTotales.Reset();

            if (RdoBtnLstAgrupacion.SelectedItem.Value == "TotalGeneral" || RdoBtnLstAgrupacion.SelectedItem.Value == "TotalCia")
            { 
                ddlEstatus.Enabled = false;
                LblEstatus.Enabled = false;
            }
            else
            {
                ddlEstatus.Enabled = true;
                LblEstatus.Enabled = true;
            }
        }

        protected void ddlNomina_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReportViewerTotales.Reset();
        }

        protected void ddlClaveMovi_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReportViewerTotales.Reset();
        }

        protected void RdoBtnFechas_CheckedChanged(object sender, EventArgs e)
        {
            if (RdoBtnFechas.Checked == true)
            {
                RdoBtnAnio.Checked = false;
                ddlAnio.Enabled = false;
                txtFechaDesde.Enabled = true;
                txtFechaHasta.Enabled = true;
            }
        }

        protected void RdoBtnAnio_CheckedChanged(object sender, EventArgs e)
        {
            if (RdoBtnAnio.Checked == true)
            {
                RdoBtnFechas.Checked = false;
                ddlAnio.Enabled = true;
                txtFechaDesde.Text = "";
                txtFechaHasta.Text = "";
                txtFechaDesde.Enabled = false;
                txtFechaHasta.Enabled = false;
            }
        }

        protected void ddlEstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReportViewerTotales.Reset();
        }
    }
}