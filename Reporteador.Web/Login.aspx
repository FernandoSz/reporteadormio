﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Reporteador.Web.Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Visor CFDI</title>
    <%--<link href="css/normalize.css" rel="stylesheet" type="text/css" />--%>
    <link id="cssLogin" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/x-icon" href="http://Reporteador.WEB/favicon.ico" />
    <link rel="icon" type="image/x-icon" href="http://Reporteador.WEB/favicon.ico" /> 

    <style type="text/css">
        .txtControl {}
        .auto-style2 {
            height: 22px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
     <div style="width:100%; margin:0 auto; margin-top:0px; left: 50px;">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
     <tr>
                    <td class="login_left">
                        &nbsp;</td>
                    <td>
                    </td>
                    <td valign="top" style="vertical-align: middle">
                        <table cellpadding="0px" cellspacing="0px" style="width:100%">
                            <tr>
                                <td class="auto-style2"></td>
                            </tr>
                            <tr>
                              <td class="login_middle">
                             <table cellpadding="0px" cellspacing="0px" border="0px">
                                    
                                       
                                <tr><td colspan="2" style="height:2px"></td></tr>
                                <tr>                               
                                    <td>
                                        <span class="txtControlLogin">
                                            <asp:Localize ID="Localize1" runat="server" meta:resourcekey="Localize1Resource1"
                                                Text="Usuario:"></asp:Localize></span>
                                    </td>
                                    <td>
                                        <%--<asp:TextBox ID="txtUsuario" runat="server" CssClass="txtControl" meta:resourcekey="txtUsuarioResource1"></asp:TextBox>--%>
                                        <asp:TextBox ID="txtUsuario"  runat="server" CssClass="txtControl" meta:resourcekey="txtUsuarioResource1" Width="160"></asp:TextBox>
                                    </td>
                                    <td valign="middle" align="center">
                                        <%--<asp:RequiredFieldValidator ID="rfvUsu" runat="server"  ForeColor="White" ControlToValidate="txtUsuario" Display="Dynamic"
                                         ErrorMessage="*" ValidationGroup="Login"></asp:RequiredFieldValidator>--%>
                                        <asp:RequiredFieldValidator ID="rfvUsuario" runat="server" ControlToValidate="txtUsuario"
                                            ErrorMessage="*" ForeColor="Red" ValidationGroup="login"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr><td colspan="2" style="height:2px"></td></tr>
                                <tr>
                                    <td>
                                        <span class="txtControlLogin">
                                            <asp:Localize ID="Localize2" runat="server" meta:resourcekey="Localize2Resource1"
                                                Text="Contraseña:   "></asp:Localize></span>
                                    </td>
                                    <td>
                                        <%--<asp:TextBox ID="txtContrasena" runat="server" CssClass="txtControl" TextMode="Password"
                                            meta:resourcekey="txtContrasenaResource1"></asp:TextBox>--%>
                                        <asp:TextBox ID="txtContrasena" runat="server" CssClass="txtControl" TextMode="Password"
                                            meta:resourcekey="txtContrasenaResource1" Width="160"></asp:TextBox>
                                    </td>
                                    <td valign="middle" align="center">
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  ControlToValidate="txtContrasena" Display="Dynamic"
                                         ErrorMessage="*" ValidationGroup="Login" ForeColor="White"></asp:RequiredFieldValidator>--%>
                                         <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtContrasena" Display="Dynamic"
                                            ErrorMessage="*" ForeColor="Red" ValidationGroup="login"></asp:RequiredFieldValidator>
                                     </td>
                                </tr>
                                 <tr><td colspan="2" style="height:2px">
                                     <br />
                                     </td></tr>
                                 <tr><td colspan="2" style="height:2px; text-align: right;">
                                        <asp:Button ID="btnLogin" runat="server" Text="Iniciar Sesión" ValidationGroup="login" OnClick="btnLogin_Click" style="cursor:pointer" Font-Size="Small"/>
                                     </td></tr>
                                <asp:Label ID="lblError" runat="server" Visible="false" ForeColor="Red" Font-Size="10px"></asp:Label>
                            </table>
                                </td>
                            </tr>
                            <%--<tr><td colspan="2" style="height:2px"></td>

                            </tr>--%>
                            <tr>
                                <td class="login_bott" valign="top" align="center">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <%--<asp:Button ID="btnLogin" runat ="server" Text ="Aceptar"  OnClick="btnSubmit_Click" ValidationGroup="Login"  style="cursor:pointer" />--%>
                                    </td>
                                    <td valign="middle">
                                      
                                    </td>
                                </tr>
                                <tr><td colspan="2" style="height:5px"></td></tr>
                                <%--<tr><td colspan="2">
                                <asp:Label ID="lblversion" runat="server" CssClass="txtControlLogin" Font-Size="10px"></asp:Label>
                                </td>--%></tr>
                            </table>
                                </td>
                            </tr>
                         </table>
                    </td>
                    
                </tr>
        
    </table>
    </div>
    <%--<div class="login_content">
        <div class="login_header">
            <h1>
                Iniciar Sesión:</h1>
        </div>
        <div class="login_msg">
            <asp:Panel ID="pnlServidor" runat="server" Visible="false">
            <span class="texto_login">Servidor:</span>
            <input type="text" id="txtServidor" class="txt_login" runat="server" style="width: 330px" />
            </asp:Panel>
            <span class="texto_login">Usuario:</span>
            <input type="text" id="txtUsuario" class="txt_login" runat="server" style="width: 330px" />
            <asp:RequiredFieldValidator ID="rfvUsuario" runat="server" ControlToValidate="txtUsuario"
                ErrorMessage="*" ForeColor="Red" ValidationGroup="login">
            </asp:RequiredFieldValidator>
            <span class="texto_login">Contraseña:</span>
            <asp:TextBox ID="txtContrasena" runat="server" CssClass="txt_login" Width="330" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtContrasena"
                ErrorMessage="*" ForeColor="Red" ValidationGroup="login">
            </asp:RequiredFieldValidator><asp:Button ID="btnLogin" runat="server" Text="Iniciar Sesión"
                CssClass="btn_login" ValidationGroup="login" OnClick="btnLogin_Click" Width="140" />
            <asp:Label ID="lblError" runat="server" Visible="false" ForeColor="Red" Font-Size="10px"></asp:Label>
        </div>
    </div>--%>
    </form>
</body>
</html>
