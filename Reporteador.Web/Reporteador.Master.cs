﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Reporteador.Logic;
using System.Web.UI.WebControls;

namespace Reporteador.Web
{
    public partial class Reporteador : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblUsuario.Text = Sessions.InfoUsuario.UsuarioAcceso;
        }

        protected void lnbCerrarSession_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Session.Clear();
            Response.Redirect("~/Login.aspx");
        }
    }
}