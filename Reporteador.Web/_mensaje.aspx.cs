﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Reporteador.Logic;

namespace Reporteador.Web
{
    public partial class _mensaje : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Se establece el texto del error a mostrarse para el usuario
            if (String.IsNullOrEmpty(Sessions.Mensaje))
            {
                lblTextoError.Text = Constants.C_MENSAJE_DEFAULT;
                lblStackTrace.Text = lblTextoError.Text;
            }
            else
            {
                lblTextoError.Text = Sessions.Mensaje;
                Sessions.Mensaje = "";
            }
            //se establece la URL de regreso para el usuario
            if (String.IsNullOrEmpty(Sessions.URLRetorno))
            {
                hplVolver.NavigateUrl = Utilities.ObtieneKeyWebConfig("RutaSite") + Constants.C_PAGINA_DEFAULT;
            }
            else
            {
                hplVolver.NavigateUrl = Sessions.URLRetorno;
                Sessions.URLRetorno = "";
            }
        }
    }
}