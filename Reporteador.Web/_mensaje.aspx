﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="_mensaje.aspx.cs" Inherits="Reporteador.Web._mensaje" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Error CFDI</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lblTextoError" runat="server" CssClass="MsjBody"></asp:Label>
    </div>
    <div>
        <asp:Label ID="lblStackTrace" runat="server" Text="Label" CssClass="texto1"></asp:Label>
    </div>
    <div>
        <asp:HyperLink ID="hplVolver" runat="server" Text="Volver"></asp:HyperLink>
    </div>
    </form>
</body>
</html>
