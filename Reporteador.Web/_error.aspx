﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="_error.aspx.cs" Inherits="Reporteador.Web._error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error</title>
    <link href="css/normalize.css" rel="stylesheet" type="text/css" />
    <link href="css/ErrorNegro.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="error_content">
        <div class="error_header">
                 <h1>Error:</h1>
        </div>
            <div class="error_msg">

            <asp:Label ID="lblTextoError" runat="server"></asp:Label>
            <div class="error_detail">
                <asp:Label ID="lblStackTrace" runat="server" Text="Label"></asp:Label>
            </div>
            </div>
        <div class="error_footer">
            <asp:HyperLink ID="hplVolver" runat="server"  title="Regresar"> <img src="images/regresar_error.png" />
                        </asp:HyperLink>
        </div>
    </div>

        
   
    
            
    </form>
</body>
</html>


